import sys
import gzip
import re
import logging

# Map chromosomes to integers (for sorting) #

chroms = {
    '1':1, '2':2, '3':3, '4':4, '5':5, '6':6, '7':7, '8':8, '9':9, '10':10, '11':11, '12':12, '13':13, '14':14, '15':15, '16':16, '17':17, '18':18, '19':19, '20':20, '21':21, '22':22, 'X':23, 'Y':24, 'MT':25, 'chr1':1, 'chr2':2, 'chr3':3, 'chr4':4, 'chr5':5, 'chr6':6, 'chr7':7, 'chr8':8, 'chr9':9, 'chr10':10, 'chr11':11, 'chr12':12, 'chr13':13, 'chr14':14, 'chr15':15, 'chr16':16, 'chr17':17, 'chr18':18, 'chr19':19, 'chr20':20, 'chr21':21, 'chr22':22, 'chrX':23, 'chrY':24, 'chrM':25, 'GL000207.1':26, 'GL000226.1':27, 'GL000229.1':28, 'GL000231.1':29, 'GL000210.1':30, 'GL000239.1':31, 'GL000235.1':32, 'GL000201.1':33, 'GL000247.1':34, 'GL000245.1':35, 'GL000197.1':36, 'GL000203.1':37, 'GL000246.1':38, 'GL000249.1':39, 'GL000196.1':40, 'GL000248.1':41, 'GL000244.1':42, 'GL000238.1':43, 'GL000202.1':44, 'GL000234.1':45, 'GL000232.1':46, 'GL000206.1':47, 'GL000240.1':48, 'GL000236.1':49, 'GL000241.1':50, 'GL000243.1':51, 'GL000242.1':52, 'GL000230.1':53, 'GL000237.1':54, 'GL000233.1':55, 'GL000204.1':56, 'GL000198.1':57, 'GL000208.1':58, 'GL000191.1':59, 'GL000227.1':60, 'GL000228.1':61, 'GL000214.1':62, 'GL000221.1':63, 'GL000209.1':64, 'GL000218.1':65, 'GL000220.1':66, 'GL000213.1':67, 'GL000211.1':68, 'GL000199.1':69, 'GL000217.1':70, 'GL000216.1':71, 'GL000215.1':72, 'GL000205.1':73, 'GL000219.1':74, 'GL000224.1':75, 'GL000223.1':76, 'GL000195.1':77, 'GL000212.1':78, 'GL000222.1':79, 'GL000200.1':80, 'GL000193.1':81, 'GL000194.1':82, 'GL000225.1':83, 'GL000192.1':84
    }


class ReadIdHolder:
    '''
    A class to hold variants by read id.
    Supports fast addition, removal by genomic position and dictionary based search for read id.
    '''
    def __init__(self, individuals, ref_chroms):
        '''
        For each individual, the class contains three objects, an array to hold reads by position, a dictionary to hold reads, by id and a dictionary to hold the number of references to a read id.
        ref_chroms holds a dictionary of chromosome ids where the value indicates the sorted order
        '''
        self.pos = {}
        self.read_ids = {}
        self.n_refs = {}
        self.individuals = individuals
        for indiv in individuals:
            self.pos[indiv] = []
            self.read_ids[indiv] = {}
            self.n_refs[indiv] = {}
        self.chroms = ref_chroms
        return

    def add(self, indiv, r_id_list, r_chrom, r_pos, r_allele):
        '''
        Add a list of reads to the holder class.
        '''
        if not r_id_list:
            return
        r_pos = int(r_pos)
        if self.pos[indiv] and self.pos[indiv][0]:
            last_chr, last_pos, tmp = self.pos[indiv][-1]
        else:
            last_chr, last_pos, tmp = '0', '0', '-'
        if r_chrom == last_chr and r_pos == last_pos:
            self.pos[indiv][-1][2] += r_id_list
        else:
            self.pos[indiv].append( [r_chrom, r_pos, r_id_list ] )
        for r_id in r_id_list:
            if not r_id:
                continue
            if r_id in self.n_refs[indiv]:
                self.n_refs[indiv][r_id] += 1
                self.read_ids[indiv][r_id].append( (r_chrom, r_pos, r_allele) )
            else:
                self.n_refs[indiv][r_id] = 1
                self.read_ids[indiv][r_id] = [ (r_chrom, r_pos, r_allele) ]
        return

    def remove(self, c_chrom, c_pos):
        '''
        Remove reads before the given genomic position.
        '''
        for indiv in self.individuals:
            while (self.pos[indiv] and self.pos[indiv][0] and (
                   self.chroms[self.pos[indiv][0][0]] < self.chroms[c_chrom] or
                   (self.chroms[self.pos[indiv][0][0]] == self.chroms[c_chrom] and 
                    self.pos[indiv][0][1] < int(c_pos)))):
                # Remove refs to reads at the top position #
                for r_id in self.pos[indiv][0][2]:
                    self.n_refs[indiv][r_id] -= 1
                    if self.n_refs[indiv][r_id] <= 0:
                        self.read_ids[indiv].pop(r_id)
                self.pos[indiv].pop(0)
        return
        
    def remove_all(self):
        '''
        Remove all reads (like at the end of a chromosome).
        '''
        for indiv in self.individuals:
            self.pos[indiv] = []
            self.read_ids[indiv] = {}
            self.n_refs[indiv] = {}
        return

    def get_r_id(self, indiv, r_id):
        '''
        Get a variants for a particular read id
        '''
        if r_id in self.read_ids[indiv]:
            return self.read_ids[indiv][r_id]
        else:
            return False

def vars_read_txt(infile):
    '''
    Read a list of variants
    '''
    var_list = []
    indivs = set()
    with open(infile, 'r') as f:
        for line in f:
            line = line.rstrip().split()
            # Check input #
            if not line[0] in chroms:
                print("Error: " + line[0] + " is not a recognized chromosome.", file=sys.stderr)
                sys.exit(1)
            if (var_list):
                if chroms[line[0]] < chroms[var_list[-1][0]]:
                    logging.error("Chromsome " + line[0] + " comes before chromsome " + indivs[line[2]][-1][0])
                    sys.exit(1)
                if line[0] == var_list[-1][0] and int(line[1]) < int(var_list[-1][1]):
                    logging.error("Input is not sorted at: " + line[0] + ':' + line[1], file=sys.stderr)
                    sys.exit(1)
            var_list.append(line)
            indivs.add(line[2])
    return (indivs, var_list)

def print_phase(var, read_ids, ped_map, parents_map, args):
    '''
    Given reads supporting a de novo variant, and reads supporting a 
    neighboring heterozygous variant, phase the variants and determine
    if the de novo variant is mosaic.
    '''
    logging.debug('\n'.join([str(var[:3]), str(read_ids), str(parents_map)]))
    logging.debug(str(read_ids.pos))

    if len(var) < 4:
        if ped_map:
            print('\t'.join(var[:3] + ['-', '-', '-']), file=args.outfile)
        else:
            print('\t'.join(var[:3] + ['-', '-']), file=args.outfile)
        return

    var_dict = {} # Key is inherited SNP chrom:pos, value is 2x2 table:
    #
    #      (index numbers are shown)
    #                 SNP
    #            | Ref | Alt |
    #            --------------
    #        Ref |  0  |  1  |
    # Mosaic ------------------
    #        Alt |  2  |  3  |
    #            --------------
    #
    # Diagonals are allowed.
    # The presence of the alternate allele of the mosaic read on the wrong 
    #   haplotype indicates an error.
    # The presence of the reference allele of the mosaic read on the wrong 
    #   haplotype indicates mosaicism.

    indiv = var[2]
    for r in var[3]: # reads supporting ref at de novo position
        supported_vars = read_ids.get_r_id(indiv, r)
        if supported_vars:
            for snp in supported_vars:
                chrom, pos, allele = snp
                pos = str(pos)
                if not (chrom + ':' + pos in var_dict):
                    var_dict[chrom + ':' + pos] = [0, 0, 0, 0]
                if allele == 'R':
                    var_dict[chrom + ':' + pos][0] += 1
                else:
                    var_dict[chrom + ':' + pos][1] += 1
    for r in var[4]: # reads supporting alt at de novo position
        supported_vars = read_ids.get_r_id(indiv, r)
        if supported_vars:
            for snp in supported_vars:
                chrom, pos, allele = snp
                pos = str(pos)
                if not (chrom + ':' + pos in var_dict):
                    var_dict[chrom + ':' + pos] = [0, 0, 0, 0]
                if allele == 'R':
                    var_dict[chrom + ':' + pos][2] += 1
                else:
                    var_dict[chrom + ':' + pos][3] += 1

    logging.debug(str(var_dict))
    # Calculate haplotypes #
    is_mosaic = False
    n_ref_haplo = 0
    n_alt_haplo = 0
    parent_arr = [] # 0 = fa, 1 = mo
    if args.details:
        print(str(var[0]) + '\t' + str(var[1]), file=args.details)
    for snp, table in var_dict.items():
        if table[2] > table[3]:
            j = 2
            if table[3] > 1: # Indicates an error in data
                logging.error("Possible genotyping error at: " + snp)
        else:
            j = 3
            if table[2] > 1:
                logging.error("Possible genotyping error at: " + snp)
        if (table[j-2] > args.min_phs_evi + args.min_phs_fraction * (table[j - 2] + table[j]) and
             table[j] >= args.min_alt_dp):
            is_mosaic = True
        n_ref_haplo += table[j-2]
        n_alt_haplo += table[j]

        parent = ''
        if ped_map:
            fa = ped_map[indiv][0]
            mo = ped_map[indiv][1]
            if (fa in parents_map and str(snp) in parents_map[fa] and
                mo in parents_map and str(snp) in parents_map[mo]):
                fa_gt = parents_map[fa][str(snp)]
                mo_gt = parents_map[mo][str(snp)]
                logging.debug(str([fa, fa_gt, mo, mo_gt]))
                if j == 2:
                    if fa_gt == 2 and mo_gt == 2:
                        logging.error("Mendelian inconsistency at adjacet variant " + snp)
                        parent = '-'
                    elif fa_gt <= 1 and mo_gt == 2:
                        parent = "Father"
                        parent_arr.append(0)
                    elif fa_gt == 2 and mo_gt <= 1:
                        parent = "Mother"
                        parent_arr.append(1)
                    else:
                        parent = '-'
                else:
                    if fa_gt == 0 and mo_gt == 0:
                        logging.error("Mendelian inconsistency at adjacet variant " + snp)
                        parent = '-'
                    elif fa_gt >= 1 and mo_gt == 0:
                        parent = "Father"
                        parent_arr.append(0)
                    elif fa_gt == 0 and mo_gt >= 1:
                        parent = "Mother"
                        parent_arr.append(1)
                    else:
                        parent = '-'

        if args.details:
            if parent:
                print('\t' + str(snp) + '\t' + '\t'.join([str(x) for x in table] + [parent]), file=args.details)
            else:
                print('\t' + str(snp) + '\t' + '\t'.join([str(x) for x in table]), file=args.details)

    if ped_map:
        fa_cnt = parent_arr.count(0)
        mo_cnt = parent_arr.count(1)
        if fa_cnt == 0 and mo_cnt == 0:
            parent = '-'
        elif fa_cnt > 1 and mo_cnt == 0:
            parent = "Father"
        elif fa_cnt == 0 and mo_cnt > 1:
            parent = "Mother"
        else:
            logging.error("Evidence for both the maternal and paternal haplotypes found at " + snp)
            parent = '-'
    if is_mosaic and n_ref_haplo + n_alt_haplo > args.min_dp:
        res = var[:3] + ["True", str(n_alt_haplo / float(n_ref_haplo + n_alt_haplo))]
    else:
        if n_alt_haplo > args.min_dp:
            res = var[:3] + ["False", '-']
        else:
            res = var[:3] + ['-', '-']

    if ped_map:
        print('\t'.join(res + [parent]), file=args.outfile)
    else:
        print('\t'.join(res), file=args.outfile)

def read_ped(ped_fn):
    '''
    Read a ped file and return a map with children as keys and (fa, mo) as values.
    Will throw an error if multiple individuals have the same identifier.
    '''
    seen = set()
    ped_map = {}
    with open(ped_fn) as f:
        for line in f:
            line = line.rstrip().split()
            if line[1] in seen:
                logging.error("Individual identifer " + line[1] + " found twice in ped file. Exiting")
                sys.exit(1)
            seen.add(line[1])
            if line[2] != '0' and line[3] != '0':
                ped_map[line[1]] = (line[2], line[3])
    return ped_map

def phase_vars(indivs, var_list, ped_map, parents_map, args):
    if not var_list: # Nothing to do
        return
    col_map = {}
    indiv_found = set()
    idx_start, idx_stop = 0, 0
    read_ids = ReadIdHolder(indivs, chroms)

    with open(args.vcf, 'rb') as f:
        # Check gzip magic number #
        if f.peek(2)[0:2] == b'\x1f\x8b':
            is_gz = True
        else:
            is_gz = False
    
    if is_gz:
        f = gzip.open(args.vcf, 'rb')
        def my_rl():
            return f.readline().decode().rstrip()
    else:
        f = open(args.vcf, 'r')
        def my_rl():
            return f.readline().rstrip()

    # Read VCF header #
    vcf_line = my_rl()
    while vcf_line and vcf_line.startswith('##'):
        vcf_line = my_rl()
    vcf_line = vcf_line.split('\t')
    for i, indiv in enumerate(vcf_line[9:]):
        i += 9
        col_map[i] = indiv
        indiv_found.add(indiv)
    logging.debug("Individuals: " + str(indiv_found))
    if not indivs <= indiv_found:
        not_found = indivs - indiv_found
        logging.error("Error: vcf does not contain individuals: " + ','.join(not_found))
        sys.exit(1)
    if ped_map:
        not_found = set()
        for indiv in indivs:
            if not ped_map[indiv][0] in indiv_found:
                not_found.add(ped_map[indiv][0])
            if not ped_map[indiv][1] in indiv_found:
                not_found.add(ped_map[indiv][1])
        if not_found:
            logging.error("Vcf does not contain parents: " + ','.join(not_found))

    vcf_line = my_rl().split('\t')
    to_phase = True
    while to_phase:
        if (not vcf_line) or (not vcf_line[0]):
            break
        if idx_start >= len(var_list):
            break
        logging.debug("Current vcf line: " + vcf_line[0] + ':' + vcf_line[1])

        # Get to same chromosome #
        if chroms[vcf_line[0]] < chroms[var_list[idx_start][0]]:
            logging.debug("Next line chrom")
            vcf_line = my_rl().split('\t')
            continue
        elif chroms[vcf_line[0]] > chroms[var_list[idx_start][0]]:
            while (idx_start < len(var_list) and
                   chroms[vcf_line[0]] > chroms[var_list[idx_start][0]]):
                print_phase(var_list[idx_start], read_ids, ped_map, parents_map, args)
                idx_start += 1
            if idx_start >= len(var_list):
                break
            if idx_stop < idx_start:
                idx_stop = idx_start
            read_ids.remove_all()
            continue
        
        # Get to same position #
        if int(vcf_line[1]) + args.distance < int(var_list[idx_start][1]):
            logging.debug("Next line pos")
            vcf_line = my_rl().split('\t')
            continue
        elif int(vcf_line[1]) - args.distance > int(var_list[idx_start][1]):
            while (idx_start < len(var_list) and
                   int(vcf_line[1]) - args.distance > int(var_list[idx_start][1])):
                print_phase(var_list[idx_start], read_ids, ped_map, parents_map, args)
                idx_start += 1
            if idx_start >= len(var_list):
                break
            if idx_stop < idx_start:
                idx_stop = idx_start
            read_ids.remove(vcf_line[0], int(vcf_line[1]) - args.distance)
            continue

        # Set idx_stop #
        while (idx_stop + 1 < len(var_list) and 
               chroms[vcf_line[0]] == chroms[var_list[idx_stop + 1][0]] and 
               int(vcf_line[1]) + args.distance > int(var_list[idx_stop][1])):
            idx_stop += 1

       # VCF line is within distance of 1 or more variants #
        gt_idx, alt_ids, ref_ids = -1, -1, -1
        for i, tag in enumerate(vcf_line[8].split(':')):
            if tag == "GT":
                gt_idx = i
            elif tag == "AltIDs":
                alt_ids = i
            elif tag == "RefIDs":
                ref_ids = i
        if gt_idx < 0 or alt_ids < 0 or ref_ids < 0:
            logging.info("Not all required genotype tags found at: " + vcf_line[0] + ':' + vcf_line[1])
            vcf_line = my_rl().split('\t')
            continue
        alleles = [vcf_line[3]] + vcf_line[4].split(',')
        
        for i, geno in enumerate(vcf_line[9:]):
            i += 9
            geno = geno.split(':')
            gt = re.split('[/|]', geno[gt_idx])
            logging.debug("Col {i}; gt {gt}".format(i=i, gt=gt))
            if (not gt or not gt[0] or gt[0] == '.'):
                continue
            cur_indiv = col_map[i]
            if cur_indiv in parents_map:
                parents_map[cur_indiv][vcf_line[0] + ':' + vcf_line[1]] = 2 - gt.count('0')
            if not cur_indiv in indivs:
                continue
            # Is a het variant called? #
            if (gt[0] == gt[1]):
                continue
            if ref_ids >= len(geno) or alt_ids >= len(geno):
                # This is a bug with the genotype annotation.                        #
                #  If no read ids are found, the annotation is not given a position. #
                continue
            # Is this a de novo position + individual? #
            denovo = False
            for j in range(idx_start, idx_stop + 1):
                if (cur_indiv == var_list[j][2] and
                     int(vcf_line[1]) == int(var_list[j][1]) and
                     chroms[vcf_line[0]] == chroms[var_list[j][0]]):
                    # Remove trailing blank read ids #
                    var_list[j].append(geno[ref_ids].split(','))
                    var_list[j].append(geno[alt_ids].split(','))
                    denovo = True
                    break
            if denovo:
                continue

            # Not a de novo position + individual #

            read_ids.add(cur_indiv, geno[ref_ids].split(','), vcf_line[0],
                         vcf_line[1], 'R')
            read_ids.add(cur_indiv, geno[alt_ids].split(','), vcf_line[0],
                         vcf_line[1], 'A')
        
        vcf_line = my_rl().split('\t')

    while (idx_start < len(var_list)):
        print_phase(var_list[idx_start], read_ids, ped_map, parents_map, args)
        idx_start += 1

    return

def main(args):
    if args.debug:
        logging.basicConfig(level=logging.DEBUG)
    else:
        logging.basicConfig(level=logging.INFO)
    indivs, var_list = vars_read_txt(args.denovo)
    ped_map = None
    parents_map = dict()
    if args.ped:
        ped_map = read_ped(args.ped)
        not_found = set()
        parents_map = {}
        for indiv in indivs:
            if indiv in ped_map:
                parents_map[ped_map[indiv][0]] = {}
                parents_map[ped_map[indiv][1]] = {}
            else:
                not_found.add(indiv)
        if not_found:
            logging.error("Individuals " + ','.join(not_found) + " are missing from the ped file")
            sys.exit(1)
    phase_vars(indivs, var_list, ped_map, parents_map, args)
