package org.kennedykrieger.freedd.ReadIds;

import org.broadinstitute.gatk.tools.walkers.annotator.interfaces.GenotypeAnnotation;
import org.broadinstitute.gatk.tools.walkers.annotator.interfaces.ExperimentalAnnotation;
import org.broadinstitute.gatk.utils.refdata.RefMetaDataTracker;
import org.broadinstitute.gatk.tools.walkers.annotator.interfaces.AnnotatorCompatible;
import org.broadinstitute.gatk.utils.contexts.ReferenceContext;
import org.broadinstitute.gatk.utils.contexts.AlignmentContext;
import htsjdk.variant.variantcontext.VariantContext;
import htsjdk.variant.variantcontext.Genotype;
import htsjdk.variant.variantcontext.GenotypeBuilder;
import org.broadinstitute.gatk.utils.genotyper.PerReadAlleleLikelihoodMap;
import htsjdk.variant.variantcontext.Allele;
import org.broadinstitute.gatk.utils.genotyper.MostLikelyAllele;
import org.broadinstitute.gatk.utils.sam.GATKSAMRecord;
import htsjdk.variant.vcf.VCFFormatHeaderLine;
import htsjdk.variant.vcf.VCFHeaderLineType;
import org.broadinstitute.gatk.utils.QualityUtils;

import java.util.List;
import java.util.Map;
import java.util.Collections;
import java.util.Arrays;

/*
 * Add the genotype annotation of comma separated read-ids to the variant.
 */

public class VariantReadIds extends GenotypeAnnotation implements ExperimentalAnnotation {

    private final static List<String> keyNames = Collections.unmodifiableList(Arrays.asList("RefIDs", "AltIDs"));

    private final static List<VCFFormatHeaderLine> descriptors = Collections.unmodifiableList(Arrays.asList( new VCFFormatHeaderLine("RefIDs", 1, VCFHeaderLineType.String, "A comma-separated list of reads supporting the reference allele"), new VCFFormatHeaderLine("AltIDs", 1, VCFHeaderLineType.String, "A comma-separated list of reads supporting the alternate allele") ));

    @Override
    public void annotate(final RefMetaDataTracker tracker,
             final AnnotatorCompatible walker,
             final ReferenceContext ref,
             final AlignmentContext stratifiedContext,
             final VariantContext vc,
             final Genotype g,
             final GenotypeBuilder gb,
             final PerReadAlleleLikelihoodMap alleleLikelihoodMap){

        if ( g == null || !g.isCalled() || !g.isHet() || ( stratifiedContext == null && alleleLikelihoodMap == null) )
            return;

        String refReadIds = "";
        String altReadIds = "";
        final List<Allele> alleles = vc.getAlleles();
        final int refLoc = vc.getStart();

        for ( final Map.Entry<GATKSAMRecord, Map<Allele,Double>> el : alleleLikelihoodMap.getLikelihoodReadMap().entrySet() ) {
            final MostLikelyAllele a = PerReadAlleleLikelihoodMap.getMostLikelyAllele(el.getValue());
            if ( ! a.isInformative() )
                continue; // read is non-informative

            final GATKSAMRecord read = el.getKey();
            if ( isUsableRead(read, refLoc) ) {
                final String id = read.getReadName();
                if ( id == null )
                	continue;
        
                if ( a.getMostLikelyAllele().isReference() ) {
                    if ( refReadIds.isEmpty() )
                        refReadIds = id.replace(":", "-");
                    else
                        refReadIds = refReadIds + "," + id.replace(":", "-");
                } else if ( alleles.contains(a.getMostLikelyAllele()) ) {
                    if ( altReadIds.isEmpty() )
                        altReadIds = id.replace(":", "-");
                    else
                        altReadIds = altReadIds + "," + id.replace(":", "-");
                }
            }
        }

        gb.attribute(getKeyNames().get(0), Arrays.asList(refReadIds));
        gb.attribute(getKeyNames().get(1), Arrays.asList(altReadIds));
    }
    
    @Override
    public List<String> getKeyNames() {
        return keyNames;
    }

    @Override
    public List<VCFFormatHeaderLine> getDescriptions() {
        return descriptors;
    }

    protected boolean isUsableRead(final GATKSAMRecord read, final int refLoc) {
        return !( read.getMappingQuality() == 0 ||
          read.getMappingQuality() == QualityUtils.MAPPING_QUALITY_UNAVAILABLE );
    }
}
