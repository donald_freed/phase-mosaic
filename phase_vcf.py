import sys

if sys.version_info <= (3, 0):
    sys.stdout.write("Python 3.x required.\n")
    sys.exit(1)

import phase_vars
import argparse

def parse_args():
    parser = argparse.ArgumentParser(description="Phase variants in a vcf to nearby heterozygous variants", formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('--outfile', help='The output file to write to [stdout].')
    parser.add_argument('--details', help='A file to write phasing details to [null].')
    parser.add_argument('--min_phs_evi', type=int, default=2, help='The minimum phasing evidence to consider a site supporting mosaicism.')
    parser.add_argument('--min_phs_fraction', type=float, default=0.01, help='The minimum fraction of reads supporting mosaicism.')
    parser.add_argument('--min_alt_dp', type=int, default=2, help="Minimum number of reads supporting the de novo variant.")
    parser.add_argument('--min_dp', type=int, default=6, help="Minimum depth to perform phasing.")
    parser.add_argument('--distance', type=int, default=1000, help="Attempt to phase de novo events to variants this far away.")
    parser.add_argument('--ped', help="A file containing pedigree information to determine parental origin")
    parser.add_argument("--debug", action="store_true", help="Turn on debugging information")
    parser.add_argument('denovo', help='A file with a list of de novo variants')
    parser.add_argument('vcf', help='A vcf file')
    return parser.parse_args()

if __name__ == "__main__":
    args = parse_args()
    if args.outfile:
        args.outfile = open(args.outfile, 'w')
    else:
        args.outfile = sys.stdout
    if args.details:
        args.details = open(args.details, 'w')

    phase_vars.main(args)
