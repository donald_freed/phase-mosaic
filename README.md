# phase-mosaic: assess mosaicism using phasing #

Phase *de novo* variants to nearby heterozygous variants to determine if they are mosaic.

### Download and Setup ###
Clone the repository to your local machine.
```
#!bash
git clone https://bitbucket.org/donald_freed/phase-mosaic.git
```

Read-backed phasing requires identifers for sequence reads supporting the reference and alternate alleles at heterozygous sites. However, this information is not provided in the standard VCF format. This repository conatins a GATK genotype annotation (VariantReadIds.java) which can be used with GATK variant callers to provide this information.

Download the GATK (if you agree to the license) and checkout release 3.5.
```
#!bash
git clone https://github.com/broadgsa/gatk-protected.git
cd gatk-protected/
git checkout tags/3.5
```

Copy the genotype annotation into the GATK source.
```
#!bash
cp /path-to-phase-mosaic/phase-mosaic/VariantReadIds.java /path-to-gatk/gatk-protected/protected/gatk-tools-protected/src/main/java
/org/broadinstitute/gatk/tools/walkers/annotator/
```

Compile the GATK. This requires JDK 7+ and maven.
```
#!bash
cd /path-to-gatk/gatk-protected/
mvn install
```

### Input Files ###
GATK variant callers require as input sorted and indexed [BAM files](https://samtools.github.io/hts-specs/SAMv1.pdf) which have read groups and marked duplicates. More information on preparing input files to the GATK can be found in the [GATK forums](https://www.broadinstitute.org/gatk/guide/article?id=1317).

Once you have a BAM file, you can run a GATK variant caller with the `VariantReadIds` annotation (`-A VariantReadIds`) to produce a VCF with information on sequence reads supporting the reference and alternate alleles.

*Note:* VCFs produced with the `VariantReadIds` annotation can be very large. Therefore, using the `-L` argument to limit variant calling to smaller regions of interest is recommended.

Example:
```
#!bash
java -jar /path-to-gatk/gatk-protected/target/GenomeAnalysisTK.jar -T HaplotypeCaller -L 2:119545190-119547190 -L 2:121890126-121892126 -L 2:123267398-123269398 -L 2:123324960-123326960 -R my_ref.fa -I my_input.bam -o variants.vcf -A VariantReadIds
```

The phasing program requires another input file containing the position of *de novo* variants. This is a flat text file with three columns. The first column indicates chromosome, the second position, and the third the sample which the variant appears in. The sample *must* match a sample name in the VCF header.

Example:
```
#!text
2       119546190       NA12878
2       121891126       NA12878
2       123268398       NA12878
2       123325960       NA12878
```

### Running ###
Simple usage:
```
#!bash
python3 /path-to-phase-mosaic/phase-mosaic/phase_vcf.py var_list.txt variants.vcf > output.txt
```

### Output Files ###
The output is a five column tab-delimited text file. The first three columns correspond to the columns of `var_list.txt`. Column four indicates whether the variant could be phased and if phasing indicates the presence of mosaicism. A '-' indicates insufficient  data to perform phasing, 'False' indicates phasing was performed and insufficient evidence for mosaicism was found, 'True' indicates phasing was performed and mosaicism was found. If phasing indicates mosaicism, the fifth column will indicate the estimated fraction of mosaic cells in the sample.

Example:
```
#!text
2       119546190       NA12878 -       -
2       121891126       NA12878 False   -
2       123268398       NA12878 -       -
2       123325960       NA12878 True    0.7058823529411765
```

If the `--details` argument is given, an additional file is produced describing the details of the phasing process. The first two columns of the file indicate the chromosome and position of *de novo* variants. If a heterozygous variant can be phased to the *de novo* variant, it will appear below the *de novo* variant with the first column empty, the chromsome:position in the second column, and columns three through six displaying data on sequence reads covering both the *de novo* allele and the inherited allele, as shown below.

```
#!text

      (index numbers are shown)
              inherited
            | Ref | Alt |
            --------------
        Ref |  3  |  4  |
 de novo ------------------
        Alt |  5  |  6  |
            --------------
```

Example:
```
#!text
2       119546190
2       121891126
        2:121891174     0       18      11      0
        2:121891176     0       18      11      0
        2:121890883     0       6       5       0
        2:121891180     0       16      11      0
2       123268398
        2:123268474     9       0       0       5
2       123325960
        2:123326018     5       12      12      0
```

### Optional Arguments ###
```
#!text
--outfile            Write the output to this file instead of stdout

--details            Write phasing details to this file

--min_phs_evi        Minimum number of reads supporting mosaicism for a 'True' call.

--min_phs_fraction   Minimum fraction of reads supporting mosaicism for a 'True' call.

--min_alt_dp         Minimum number of phased reads supporting the *de novo* variant on the mosaic haplotype.

--min_dp             Minimum number of phased reads supporting the mosaic haplotype.

--distance           Maximum distance to phase variants (larger values use more memory).

--ped PED             A file containing pedigree information to determine parental origin of the de novo mutations (default: None)

--debug               Turn on debugging information (default: False)
```